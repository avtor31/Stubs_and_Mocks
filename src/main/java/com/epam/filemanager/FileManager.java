package com.epam.filemanager;


public class FileManager {
    private static final int MIN_FILE_LENGTH = 3;

    private LogService logService;

    public FileManager(LogService service) {
        this.logService = service;
    }

    public void analyze(String fileName) {
        if(fileName.length() <= MIN_FILE_LENGTH) {
            logService.logError("FileNameLessThenMinException");
        }
    }
}
