package com.epam.filemanager;

public class ConsoleLogService implements LogService {
    private String lastError;

    @Override
    public void logError(String error) {
        this.lastError = error;
        logIntoConsole(error);
    }



    @Override
    public String getLastErrorMessage() {
        return lastError;
    }

    @Override
    public void clear() {

    }

    private void logIntoConsole(String error) {

        System.out.println(error);
    }
}
