package com.epam.filemanager;

public interface LogService {
    void logError(String error);
    String getLastErrorMessage();
    void clear();
}
