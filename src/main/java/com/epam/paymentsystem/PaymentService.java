package com.epam.paymentsystem;

public interface PaymentService {

    User findUser(String cardId);

    void makePayment (String orderNumber, User user);

    boolean cancelOrder(String orderNumber);

}
