package com.epam.paymentsystem;

public class UserManager {
    private PaymentService paymentService;

    public UserManager(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    public String getUserFullName(String cardId) {
        User user = this.paymentService.findUser(cardId);
        return user == null ? null : user.getFirstName() + " " + user.getLastName();
    }
}
