package com.epam.filemanager;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import com.epam.filemanager.FileManager;
import com.epam.filemanager.LogService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

/**
 * Unit test for simple App.
 */
public class FileManagerTest {
    private FileManager fileManager;
    private LogService fakeLogService;
    private Answer answer;

    private String fileName;

    @Before
    public  void initTest(){
        fakeLogService = mock(LogService.class); // will be used as mock

        fileManager = new FileManager(fakeLogService);
    }


    @Test
    public void analyzeMoreThenThreeFileNameTest() {
        fileManager.analyze("SomeFile");
        verify(fakeLogService, times(0)).logError("FileNameLessThenMinException");
    }

    @Test
    public void analyzeLesssThenThreeFileNameTest() {
        fileManager.analyze("af");
        verify(fakeLogService, times(1)).logError("FileNameLessThenMinException");
    }
}
