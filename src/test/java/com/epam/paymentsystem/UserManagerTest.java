package com.epam.paymentsystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserManagerTest {
    private UserManager userManager;
    private User user;
    private PaymentService fakePaymentService;

    @Before
    public void setup() {
        userSetup();
        fakePaymentService = mock(PaymentService.class);
        userManager = new UserManager(fakePaymentService);
        setStubAnswers();

    }

    private void setStubAnswers() {
        when(fakePaymentService.findUser("1")).thenReturn(user);
        when(fakePaymentService.findUser("2")).thenReturn(null);
    }

    private void userSetup() {
        user = new User();
        user.setCardId("1");
        user.setFirstName("Nick");
        user.setLastName("Cave");
    }

    @Test
    public void successfullyFindUser() {
        String fullUserName = userManager.getUserFullName("1");
        assertEquals("Nick Cave", fullUserName);
    }

    @Test
    public void unsuccessfullyFindUser() {
        String fullUserName = userManager.getUserFullName("2");
        assertFalse("Nick Cave".equals(fullUserName));
        assertTrue(null == fullUserName);
    }
}
